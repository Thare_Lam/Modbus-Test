from Tkinter import *
import tkMessageBox
import socket
import binascii


class ModFun:

    fclist = ['01', '02', '03', '04', '05', '06', '0f', '10', '16', '17', '2b']

    basicparameter = ['Status', 'Transaction Identifier', 'Protocol Identifier', 'Length',
                            'Unit Identifier', 'Function Code']

    sendparametersvector = [
        ['Reference Number', 'Bit Count'],
        ['Reference Number', 'Bit Count'],
        ['Reference Number', 'Word Count'],
        ['Reference Number', 'Word Count'],
        ['Reference Number', 'Data'],
        ['Reference Number', 'Data'],
        ['Reference Number', 'Bit Count', 'Byte Count', 'Data'],
        ['Reference Number', 'Word Count', 'Byte Count', 'Data'],
        ['Reference Number', 'AND mask', 'OR mask'],
        ['Read Reference Number', 'Read Word Count', 'Write Reference Number',
         'Write Word Count', 'Byte Count', 'Data'],
        ['MEI Type', 'Read Device ID', 'Object ID']
    ]

    recvparametersvector = [
        ['Bit Count', 'Data'],
        ['Bit Count', 'Data'],
        ['Bit Count', 'Data'],
        ['Bit Count', 'Data'],
        ['Reference Number', 'Data'],
        ['Reference Number', 'Data'],
        ['Reference Number', 'Data'],
        ['Reference Number', 'Data'],
        ['Reference Number', 'AND mask', 'OR mask'],
        ['Bit Count', 'Data'],
        ['MEI Type', 'Read Device ID', 'Conformity Level', 'More Follows',
         'Next Object ID', 'Number of Object', 'Object #1', 'Object #2', 'Object #3']
    ]

    rulervector = [[2, -1], [2, -1], [2, -1], [2, -1], [4, 4], [4, 4], [4, 4], [4, 4],
             [4, 4, 4], [2, -1], [2, 2, 2, 2, 2, 2, -1]]

    def __init__(self, address, title, index):
        self.address = address
        self.title = title
        self.index = index
        self.fc = self.fclist[index]
        self.win = None
        self.entrylist = []
        self.labellist = []

    def show(self):

        sendparameters = self.basicparameter[1:] + self.sendparametersvector[self.index]
        recvparameters = self.basicparameter + self.recvparametersvector[self.index]

        self.win = Tk()
        self.win.title(self.title)

        frmroot = Frame(self.win)

        frmroot1 = Frame(frmroot)

        frm0 = Frame(frmroot1)
        Label(frm0, text='Send:', font=('Arial', 20)).pack(side=LEFT, anchor=W)
        frm0.pack(side=TOP, anchor=W)

        frm1 = Frame(frmroot1)
        frm = Frame(frm1)
        for parameter in sendparameters:
            Label(frm, text=parameter, font=('Arial', 20)).pack(side=TOP, anchor=E)
        frm.pack(side=LEFT)
        frm = Frame(frm1)
        for i in range(0, len(sendparameters)):
            entry = Entry(frm)
            self.entrylist.append(entry)
            entry.pack(side=TOP)
        frm.pack(side=RIGHT)
        frm1.pack(side=TOP)

        frm2 = Frame(frmroot1)
        Button(frm2, text="Send", width=10, command=lambda: self.btnfun()).pack()
        frm2.pack(side=TOP)
        frmroot1.pack(side=LEFT)

        ##########################################################

        frmroot2 = Frame(frmroot)
        frm3 = Frame(frmroot2)
        Label(frm3, text='Response:', font=('Arial', 20)).pack(side=LEFT, anchor=W)
        frm3.pack(side=TOP, anchor=W)

        frm4 = Frame(frmroot2)
        frm = Frame(frm4)
        for parameter in recvparameters:
            Label(frm, text=parameter, font=('Arial', 20)).pack(side=TOP, anchor=E)
        frm.pack(side=LEFT, anchor=W)
        frm = Frame(frm4)
        for i in range(0, len(recvparameters)):
            label = Label(frm, width=20, font=('Arial', 20))
            self.labellist.append(label)
            label.pack(side=TOP, anchor=E)
        frm.pack(side=RIGHT)
        frm4.pack(side=TOP, fill=X)

        frmroot2.pack(side=RIGHT)

        frmroot.pack(padx=20, pady=20)

        self.win.mainloop()

    def btnfun(self):
        self.clearlabel(0, len(self.labellist))
        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.settimeout(1)
            sock.connect(self.address)
            sendData = ''
            for i in range(0, len(self.entrylist)):
                if self.entrylist[i].get() == '':
                    if i == 0 or i == 1:
                        temp = '0000'
                    elif i == 2:
                        length = 0
                        for j in range(3, len(self.entrylist)):
                            if (j == 3 or j == 4) and self.entrylist[j].get() == '':
                                length += 1
                            else:
                                length += len(self.entrylist[j].get()) / 2
                        hexlength = hex(length)[2:]
                        temp = (4 - len(hexlength)) * '0' + hexlength
                    elif i == 3:
                        temp = '00'
                    elif i == 4:
                        temp = self.fc
                    else:
                        temp = ''
                else:
                    temp = self.entrylist[i].get()
                sendData += temp
            sock.sendall(binascii.a2b_hex(sendData))
        except Exception, e:
            tkMessageBox.showerror(title='Error', message=e)
            sock.close()
            return

        try:
            recvdata = binascii.b2a_hex(sock.recv(1024))
        except Exception, e:
            self.labellist[0].config(text=e, fg='red')
            return
        finally:
            sock.close()

        self.setlable(recvdata)
        return

    def setlable(self, recvdata):
        ruler = self.rulervector[self.index]
        self.labellist[1].config(text=recvdata[0:4])
        self.labellist[2].config(text=recvdata[4:8])
        self.labellist[3].config(text=recvdata[8:12])
        self.labellist[4].config(text=recvdata[12:14])
        self.labellist[5].config(text=recvdata[14:16])
        if recvdata[14:16] != self.fc:
            self.labellist[0].config(text='Exception' + recvdata[16:18], fg='red')
        else:
            self.labellist[0].config(text='Right', fg='green')
            labnum = 6
            index = 16
            for i in range(0, len(ruler)):
                if ruler[i] == -1:
                    if self.fc == '2b':
                        data = recvdata[index:]
                        while len(data):
                            l = int(data[2:4], 16)
                            data = data[4:]
                            info = binascii.a2b_hex(data[:l*2])
                            self.labellist[labnum].config(text=info)
                            data = data[l*2:]
                            labnum += 1
                    else:
                        self.labellist[labnum].config(text=recvdata[index:])
                else:
                    self.labellist[labnum].config(text=recvdata[index:index + ruler[i]])
                labnum += 1
                index += ruler[i]

        self.win.update_idletasks()
        return

    def clearlabel(self, start, end):
        if end > len(self.labellist):
            end = len(self.labellist)
        for i in range(start, end):
            self.labellist[i].config(text='')
        self.win.update_idletasks()
