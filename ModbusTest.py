from ModFun import *

fclist = ['0x01', '0x02', '0x03', '0x04', '0x05', '0x06', '0x0f', '0x10', '0x16', '0x17', '0x2b']

fnlist = ['Read Coils', 'Read Discrete Inputs', 'Read Holding Register', 'Read Input Register',
                'Write Single Coils', 'Write Single Register', 'Write Multiple Coils',
                'Write Multiple Registers', 'Mask Write Register', 'Read/Write Multiple Register',
                'Read Device Identification']


def selectfc(event):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        time = float(timeout.get())
        sock.settimeout(time)
        address = (ip.get(), int(port.get()))
        sock.connect(address)
    except Exception, e:
        tkMessageBox.showerror(title='Error', message=e)
        return
    finally:
        sock.close()
    index = lb.curselection()[0]
    title = fclist[index] + ' - ' + fnlist[index]
    ModFun(address, title, index).show()
    return

if __name__ == '__main__':
    win = Tk()
    win.title('Modbust Test')

    frmroot = Frame(win)

    frm1 = Frame(frmroot)
    Label(frm1, text='Function Code', font=('Arial', 20)).pack(side=TOP)
    lb = Listbox(frm1, width=30, height=11)
    for i in range(0, len(fclist)):
        lb.insert(END, fclist[i] + '   ' + fnlist[i])
    lb.bind('<ButtonRelease-1>', selectfc)
    lb.pack(side=TOP)
    frm1.pack(side=LEFT)

    frm2 = Frame(frmroot)
    frm = Frame(frm2)
    Label(frm, text='IP', font=('Arial', 20)).pack(side=LEFT, anchor=W)
    ip = StringVar()
    Entry(frm, textvariable=ip, width=15).pack(side=RIGHT, anchor=E)
    frm.pack(side=TOP, fill=BOTH, padx=10, pady=20)
    frm = Frame(frm2)
    Label(frm, text='PORT', font=('Arial', 20)).pack(side=LEFT, anchor=W)
    port = StringVar()
    Entry(frm, textvariable=port, width=15).pack(side=RIGHT, anchor=E)
    port.set("502")
    frm.pack(side=TOP, fill=BOTH, padx=10, pady=20)
    frm = Frame(frm2)
    Label(frm, text='Time out', font=('Arial', 20)).pack(side=LEFT, anchor=W)
    timeout = StringVar()
    Entry(frm, textvariable=timeout, width=15).pack(side=RIGHT, anchor=E)
    timeout.set("3")
    frm.pack(side=TOP, fill=BOTH, padx=10, pady=20)
    frm2.pack(side=RIGHT, fill=BOTH)

    frmroot.pack(padx=20, pady=40)

    win.mainloop()

